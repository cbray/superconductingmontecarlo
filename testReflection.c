#include <vector>
#include <thread>
#include <iostream>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>
// #include <mutex>


using namespace std;

const double halfSideLength = 1; // Origin is in the center of the detector
const double halfHeight = 1;

/// Generic functions

// Return true if particle is outside volume, false otherwise
bool checkPosition(const vector<double>& position){

    if(abs(position[0]) > halfSideLength) return true;
    if(abs(position[1]) > halfSideLength) return true;
    if(abs(position[2]) > halfHeight) return true;
    return false;

}

vector<double> reflectPosition(vector<double> initialPosition, vector<double> deltaPosition){
    vector<double> testPosition = {initialPosition[0] + deltaPosition[0], initialPosition[1] + deltaPosition[1], initialPosition[2] + deltaPosition[2]};

    while(checkPosition(testPosition)){ // Wont execute if testPosition is good, so we can skip everything if the initial test is good, or account for multiple bounces (eg very long interaction length)
        for(int i=0;i<3;i++){ // Repeat for each axis
            if(testPosition[i] > halfSideLength){
                double delta = halfSideLength - initialPosition[i];
                cout << delta << endl;
                deltaPosition[i] = -(deltaPosition[i]-delta); // Subtract the distance already traveled in that direction, and then reflect the velocity
                initialPosition[i] = halfSideLength;
                testPosition[i] = initialPosition[i] + deltaPosition[i];
            }
            if(testPosition[i] < -halfSideLength){ // Opposite side for reflection
                double delta = -halfSideLength - initialPosition[i];
                cout << delta << endl;
                deltaPosition[i] = -(deltaPosition[i]-delta); // Subtract the distance already traveled in that direction, and then reflect the velocity
                initialPosition[i] = -halfSideLength;
                testPosition[i] = initialPosition[i] + deltaPosition[i];
            }
        }

    }

    return testPosition;
}


int main(){
    vector<double> initialPosition = {0,0,0};
    vector<double> deltaPosition = {1.5,40.5,1.3};

    vector<double> finalPosition = reflectPosition(initialPosition,deltaPosition);

    cout << finalPosition[0] << ", " << finalPosition[1] << ", " << finalPosition[2] << endl;
}
