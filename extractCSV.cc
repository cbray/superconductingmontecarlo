#include <vector>
#include <thread>
#include <iostream>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <utility> // For pair
#include <map>
#include <sys/stat.h>
#include <unistd.h>
#include <mutex>
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"
#include "CLHEP/Vector/ThreeVector.h"
#include <random>

using namespace std;
using namespace CLHEP;

vector<TH2D*> lostEnergyHistos; // x is the total energy, y is the number of escapes
vector<TH1D*> quasiparticleEnergy; // Final energy that is in quasiparticles (not including escapes)
vector<TH1D*> quasiparticleNumber; // Final number of quasiparticles (not including escapes)
vector<TH1D*> subGapPhononEnergy; // Final energy that is in phonons, which are necesarily sub-gap otherwise they would end up creating more qp
vector<TH1D*> subGapPhononNumber; // Final number of sub-gap phonons


/// Other things
const double fs = 1E-15 * second;
const double electron_mass = electron_mass_c2/c_squared;
double bindingEnergy = 0.00072 * eV;

inline bool file_exists (const std::string& name) { // Credit to stackoverflow user PherricOxide
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

int main(){

    cout << "Select material: Ta (1), Nb (2), Al (3)" << endl;
    int q; cin >> q;
    if (q == 1) {
        bindingEnergy = 0.00072 * eV;
    } else if (q == 2) {
        bindingEnergy = 0.00155 * eV;
    } else if (q == 3) {
        bindingEnergy = 0.00017 * eV;
    } else {
        cout << "Out of Bounds. Aborting." << endl;
        return 1;
    }

    vector<ifstream> dataFiles;

    int maxThreads = 0;
    while(file_exists("exitStatisticsOutputFile_t"+to_string(maxThreads)+".out")){
        dataFiles.push_back(ifstream("exitStatisticsOutputFile_t"+to_string(maxThreads)+".out", ios::binary));
        maxThreads++;
    }

    cout << maxThreads << " theads found" << endl;

    for(int i=0;i<dataFiles.size()+1;i++){
        lostEnergyHistos.push_back(new TH2D(("t"+to_string(i)+"_lostEnergyHisto").c_str(),"Total Lost Energy vs Number of Escaped Electrons",560,0,56*eV,100,0,100));
        quasiparticleEnergy.push_back(new TH1D(("t"+to_string(i)+"_quasiparticleEnergy").c_str(),"Final Total Quasiparticle Energy",10000,0,56*eV));
        quasiparticleNumber.push_back(new TH1D(("t"+to_string(i)+"_quasiparticleNumber").c_str(),"Final Number of Quasiparticles",1000000,0,1000000)); //Estimate of how many particles we will have, may need to be updated));
        subGapPhononEnergy.push_back(new TH1D(("t"+to_string(i)+"_subGapPhononEnergy").c_str(),"Final Total Sub-Gap Phonon Energy",10000,0,56*eV));
        subGapPhononNumber.push_back(new TH1D(("t"+to_string(i)+"_subGapPhononNumber").c_str(),"Final Number of Sub-Gap Phonons",1000000,0,1000000));
    }

    for(int i=0; i<dataFiles.size(); i++){
        vector<double> exitStatistics = {0,0,0,0,0,0};
        int totalEscapes = 0; int totalEvents = 0; int totalEscapeEvents = 0;
        while(!dataFiles[i].eof()){
            dataFiles[i].read((char*)(&exitStatistics[0]), 6 * sizeof(double));
            // for(int j = 0; j < 6; j++) cout << exitStatistics[j] << ", ";
            // cout << endl;
            lostEnergyHistos[i]->Fill(exitStatistics[0],exitStatistics[1]);
            quasiparticleEnergy[i]->Fill(exitStatistics[2]);
            quasiparticleNumber[i]->Fill(exitStatistics[3]);
            subGapPhononEnergy[i]->Fill(exitStatistics[4]);
            subGapPhononNumber[i]->Fill(exitStatistics[5]);
            totalEvents++;
            totalEscapes+=exitStatistics[1];
            if(exitStatistics[1]) totalEscapeEvents++;
        }
    }
    for(int i=0;i<lostEnergyHistos.size()-1;i++){ // Create sum histograms at end of list, particularly useful for when you are running the same sim on each thread
        lostEnergyHistos[lostEnergyHistos.size()-1]->Add(lostEnergyHistos[i]);
        quasiparticleEnergy[quasiparticleEnergy.size()-1]->Add(quasiparticleEnergy[i]);
        quasiparticleNumber[quasiparticleNumber.size()-1]->Add(quasiparticleNumber[i]);
        subGapPhononEnergy[subGapPhononEnergy.size()-1]->Add(subGapPhononEnergy[i]);
        subGapPhononNumber[subGapPhononNumber.size()-1]->Add(subGapPhononNumber[i]);
    }

    ofstream srimout("srim_lostEnergyHisto.csv");
    for(int i=1;i<9;i++){
        for(int j=1;j<560;j++){
            srimout << i-1  << "," << 56-(double)j/10.0 << "," << lostEnergyHistos[lostEnergyHistos.size()-1]->GetBinContent(j,i) << endl;
        }
    }
    srimout.close();

    // ofstream t8("t8_lostEnergyHisto.csv");
    // for(int i=1;i<9;i++){
        // for(int j=1;j<560;j++){
            // t8 << i-1  << "," << 56-(double)j/10.0 << "," << lostEnergyHistos[8]->GetBinContent(j,i) << endl;
        // }
    // }
    // t8.close();
    // ofstream t19("t19_lostEnergyHisto.csv");
    // for(int i=1;i<9;i++){
    //     for(int j=1;j<560;j++){
    //         t19 << i-1  << "," << 56-(double)j/10.0 << "," << lostEnergyHistos[19]->GetBinContent(j,i) << endl;
    //     }
    // }
    // t19.close();

    for(int i=0;i<lostEnergyHistos.size();i++){
        delete lostEnergyHistos[i];
        delete quasiparticleEnergy[i];
        delete quasiparticleNumber[i];
        delete subGapPhononEnergy[i];
        delete subGapPhononNumber[i];
        if(i!=lostEnergyHistos.size()-1) dataFiles[i].close();
    }

    return 0;
}
