#include <vector>
#include <thread>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <utility> // For pair
#include <map>
#include <sys/stat.h>
#include <unistd.h>
#include <mutex>
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include <random>

using namespace std;
// using namespace std::chrono;

struct trace{
    unsigned short channelIdentifier;
    unsigned short eventID0, eventID1;
    vector<unsigned short> dataWords;
};

int main(int argc, char** argv){
    ifstream fin(argv[1],std::ios::binary);

    TH1D diffHisto("diffs","diffs",2*65535, -65535,65535);
    TH1D values("vals","vals",65535,0,65535);
    TH1D compressed("compress","compress",20,0.5,20.5);
    TH1D compressed2("compress2","compress2",20,0.5,20.5);
    TH1D compressed3("compress3","compress3",20,0.5,20.5);
    TH1D diffLength("diffLength","diffLength",1000,0.5,1000.5);

    int size = 16;
    vector<unsigned short> last256; int i=0;
    for(int i=0;i<size;i++) last256.push_back(0);

    unsigned short temp; do {fin.read((char *) &temp, sizeof(temp)); /*cout << temp << ", ";*/} while(temp != 0xEEEE); // Skip to trace header
    // cout << endl;

    int j=0;
    while(!fin.eof() && j < 10000){
        trace newTrace;
        fin.read((char *) &newTrace.channelIdentifier, sizeof(newTrace.channelIdentifier));
        fin.read((char *) &newTrace.eventID0, sizeof(newTrace.eventID0));
        fin.read((char *) &newTrace.eventID1, sizeof(newTrace.eventID1));
        fin.read((char *) &temp, sizeof(temp));
        // cout << newTrace.channelIdentifier << "\t" << newTrace.eventID0 << "\t" << newTrace.eventID1 << endl;
        int len = 0;
        int diffCounter = 0;
        while(!fin.eof() && temp != 0xEEEE){
            len++;
            // cout << temp << ",";
            temp%=4096;
            newTrace.dataWords.push_back(temp);
            values.Fill(temp);
            long long int diff = (newTrace.dataWords.size() > 1)?(newTrace.dataWords[newTrace.dataWords.size()-2]-temp):temp;
            diffHisto.Fill(diff);


            if(     diff < pow(2,4-1)-1 && diff > -1*pow(2,4-1)) compressed.Fill(5);
            else if(diff < pow(2,8-1)-1 && diff > -1*pow(2,8-1)) compressed.Fill(10);
            else compressed.Fill(14);

            // cout << "finding" << endl;
            auto findIter = std::find(last256.begin(), last256.end(), temp);
            // cout << ((findIter != last256.end())?"found!":"not found") << endl;
            int diffBits = 3;
            if(diff < pow(2,diffBits-1)-1 && diff > -1*pow(2,diffBits-1)) compressed2.Fill(diffBits+1);
            else if(findIter != last256.end()) compressed2.Fill(6);
            else compressed2.Fill(14);
            if(findIter == last256.end()){
                last256[i++] = temp;
                i%=size;
            }

            diffBits = 4;
            diff/=2;
            if(diff < pow(2,diffBits-1)-1 && diff > -1*pow(2,diffBits-1)) diffCounter++;
            else{
                diffLength.Fill(diffCounter);
                diffCounter=0;
            }

            fin.read((char *) &temp, sizeof(temp));
        }
        // cout << endl;
        // traceList.push_back(newTrace);
        newTrace.dataWords.clear();
        cout << "Trace " << j++ << " complete. Length " << len << endl;
    }

    TFile f("diffHistoOut.root","RECREATE");
    diffHisto.Write("diffs", TObject::kSingleKey);
    values.Write("vals", TObject::kSingleKey);
    compressed.Write("compress", TObject::kSingleKey);
    compressed2.Write("compress2", TObject::kSingleKey);
    diffLength.Write("diffLength", TObject::kSingleKey);
    // compressed3.Write("compress3", TObject::kSingleKey);
    f.Write();
    f.Close();

    return 0;
}
