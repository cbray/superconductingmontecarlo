#include <vector>
#include <thread>
#include <iostream>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>
// #include <mutex>
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"
#include "CLHEP/Vector/ThreeVector.h"

using namespace std;
using namespace CLHEP;
using namespace std::chrono;
typedef std::chrono::high_resolution_clock Clock;

const double workFunction = 4.5 * eV;
const double maxDepth = 2; // 84;
// TF1 depthFunction("depthFunction","((x/553)**(0.829))*(exp(-(x/553)**1.829))",0,1660); // Shallow Implant
TF1 depthFunction("depthFunction","((x/1220)**(0.829))*(exp(-(x/1220)**1.829))",0,1660); // Deep Implant
const double startingEnergy = 56 * eV;

const double halfSideLength = 34 * micrometer;
const double halfHeight = 82.5 * nanometer;

const double mfpA = 9;
const double mfpB = -6.61;
const double mfpE_th = 0; // E_th = E_F, approximated to 0
const double mfpE_0 = 1; // 1 eV
const double mfpa = 0.001;
const double mfpb = 1.8;
const double arbitraryScaleFactor = 1;

const int numIterations = 100000000;
int activeThreads = 0;
const int maxThreads = 36;
const int threadsPerDepth = 36;

vector<TH1D*> lostEnergyHistos;
vector<TH2D*> scatterHistos;
vector<TH2D*> escapedHistos;

// Get random double [0,1)
double flat() {
  return double(rand()) / (double(RAND_MAX) + 1.0);
}

// Return the energy of the electron from its velocity
double getEnergy(const vector<double>& finalVelocity){

    return 0.5 * electron_mass_c2 * (finalVelocity[0]*finalVelocity[0] + finalVelocity[1]*finalVelocity[1] + finalVelocity[2]*finalVelocity[2]);

}

// Get random vector with startingEnergy energy
vector<double> getRandomStartingVelocity(){

    vector<double> startingVelocity = {flat(),flat(),flat()};
    double scalingFactor = sqrt(56*eV / (0.5 * electron_mass_c2 * (startingVelocity[0]*startingVelocity[0] + startingVelocity[1]*startingVelocity[1] + startingVelocity[2]*startingVelocity[2])));
    for(int i=0;i<3;i++) startingVelocity[i] *= scalingFactor;

    // cout << "energy test: " <<  0.5 * electron_mass_c2 * (startingVelocity[0]*startingVelocity[0] + startingVelocity[1]*startingVelocity[1] + startingVelocity[2]*startingVelocity[2]) << "\t" << getEnergy(startingVelocity) <<  "\t" << 56 * eV << endl;

    return startingVelocity;

}

// Get random vector at the starting position
vector<double> getRandomStartingPosition(int depth = -1){
    vector<double> startingPosition = {0,0,82.5*nanometer - depth*nanometer};
    if(depth >= 0) return startingPosition;
    do {
        startingPosition[0] = (flat()-0.5) * 50 * micrometer;
        startingPosition[1] = (flat()-0.5) * 50 * micrometer;
    } while(sqrt(startingPosition[0]*startingPosition[0] + startingPosition[1]*startingPosition[1]) > 25*micrometer);

    startingPosition[2] = 82.5*nanometer - depthFunction.GetRandom()*angstrom; // Draw from distribution

    return startingPosition;

}

// Calculate position of next interaction based on the initial position, velocity, and mean interaction length.
// Use exponential distribution w/ mean equal to mean interaction length
vector<double> newPosition(const vector<double>& initialPosition, const vector<double>& initialVelocity){

    double currentEnergyeV = getEnergy(initialVelocity) / eV;

    double meanFreePath = sqrt(currentEnergyeV)/(mfpa * pow(currentEnergyeV - mfpE_th,mfpb))  +  (currentEnergyeV - (mfpE_0 * exp(-mfpB/mfpA))) / (mfpA * log(currentEnergyeV/mfpE_0) + mfpB);

    meanFreePath = meanFreePath*angstrom*arbitraryScaleFactor;

    double freePath = -log(flat()) * meanFreePath; // Exponential distribution with mean of meanFreePath

    // cout << "meanFreePath: " + to_string(meanFreePath/angstrom) + " angstroms, freePath: " + to_string(freePath/angstrom) + " angstroms\n";

    vector<double> deltaPosition = initialVelocity;

    double scale = sqrt(initialVelocity[0]*initialVelocity[0] + initialVelocity[1]*initialVelocity[1] + initialVelocity[2]*initialVelocity[2]);

    for(int i=0;i<3;i++) deltaPosition[i] *= freePath/scale;

    vector<double> finalPosition = initialPosition;

    for(int i=0;i<3;i++) finalPosition[i] += deltaPosition[i];

    return finalPosition;

}

// Split the initial energy into two electrons, conserving energy and momentum.
void splitEnergy(const vector<double>& initialVelocity, vector<double>& finalVelocity1, vector<double>& finalVelocity2){

    vector<double> xAxis = initialVelocity; // Initial direction of travel
    double scale = sqrt(xAxis[0]*xAxis[0] + xAxis[1]*xAxis[1] + xAxis[2]*xAxis[2]);
    for(int i=0;i<3;i++) xAxis[i] /= scale;

    vector<double> random = {flat(),flat(),flat()}; // Generate parrelel axis at random angle by generating a random vector and crossing it with axis of travel, then rescaling it
    vector<double> yAxis = {xAxis[2]*random[3] - xAxis[3]*random[2], xAxis[3]*random[1] - xAxis[1]*random[3], xAxis[1]*random[2] - xAxis[2]*random[1]};
    scale = sqrt(yAxis[0]*yAxis[0] + yAxis[1]*yAxis[1] + yAxis[2]*yAxis[2]);
    for(int i=0;i<3;i++) yAxis[i] /= scale;

    double vx1 = -1;
    double energyeV = getEnergy(initialVelocity)/eV;
    while(vx1 < 0) vx1 = 1 + 1/(-0.5 - (7.183642032538396e28 * pow(flat(),2) * pow(4.681131805845572e-10 - (122.23367762988053/pow(510998.94610000006 + energyeV,2)),2) ) );

    scale = sqrt(initialVelocity[0]*initialVelocity[0] + initialVelocity[1]*initialVelocity[1] + initialVelocity[2]*initialVelocity[2]);
    for(int i=0;i<3;i++){
        finalVelocity1[i] = (xAxis[i] * vx1 * scale) + (yAxis[i] * sqrt(vx1 * (1-vx1)) * scale);
        finalVelocity2[i] = (xAxis[i] * (1-vx1) * scale) - (yAxis[i] * sqrt(vx1 * (1-vx1)) * scale);
    }

    return;

}

// Return true if particle is outside volume, false otherwise
bool checkPosition(const vector<double>& finalPosition){

    if(abs(finalPosition[0]) > halfSideLength) return true;
    if(abs(finalPosition[1]) > halfSideLength) return true;
    if(abs(finalPosition[2]) > halfHeight) return true;
    return false;

}

double lostEnergy(const vector<double>& initialPosition, const vector<double>& initialVelocity, vector<vector<double>> * escapes, int& numEscapes, int recursionDepth = 0){

    vector<double> finalPosition = newPosition(initialPosition,initialVelocity);

    if(checkPosition(finalPosition)){
        if(recursionDepth==0) cout << "HERE!\n";
        double lostEnergy1 = getEnergy(initialVelocity);
        if(lostEnergy1>workFunction){
            escapes->push_back({lostEnergy1,recursionDepth});
            numEscapes++;
            return lostEnergy1;
        }
        return 0;
    } else {
        vector<double> finalVelocity1 = {0,0,0};
        vector<double> finalVelocity2 = {0,0,0};

        splitEnergy(initialVelocity,finalVelocity1,finalVelocity2);

        double lostEnergy1 = 0,lostEnergy2 = 0;
        if(getEnergy(finalVelocity1) > workFunction) lostEnergy1 = lostEnergy(finalPosition,finalVelocity1,escapes,numEscapes,recursionDepth+1);
        if(getEnergy(finalVelocity2) > workFunction) lostEnergy2 = lostEnergy(finalPosition,finalVelocity2,escapes,numEscapes,recursionDepth+1);
        return lostEnergy1 + lostEnergy2;
    }

}

void threadable(int depth){
    vector<vector<double>> escapes;
    for(int i=0;i<numIterations;i++){
         vector<double> startingVelocity = getRandomStartingVelocity();
         vector<double> startingPosition = getRandomStartingPosition(depth); // {0,0,82.5*nanometer - depth*nanometer};
         escapes.clear();
         int numEscapes = 0;
         double escapeEnergy = lostEnergy(startingPosition,startingVelocity,&escapes,numEscapes);
         lostEnergyHistos[(depth>=0)?depth:0]->Fill(escapeEnergy);
         if(escapes.size() == 1) scatterHistos[(depth>=0)?depth:0]->Fill(escapes[0][0],escapes[0][1]);
         escapedHistos[(depth>=0)?depth:0]->Fill(escapeEnergy,numEscapes);
         // if(i%1000 == 0) printf("\rDepth: %3dnm, Iteration: %06d",depth,i);
    }

    activeThreads--;
}

int main(){
    srand(static_cast<unsigned int>(clock()));
    TFile *f = new TFile("lostEnergy.root","RECREATE");

/*
    lostEnergyHistos.push_back(new TH1D("lostEnergyHisto","Auger Escape Energy at Implantation Depth Distribution from SRIM",560,0,startingEnergy));
    scatterHistos.push_back(new TH2D("scatterHisto","Auger Escape Energy vs Scatter Depth at Implantation Depth Distribution from SRIM",560,0,startingEnergy,100,0,100));
    escapedHistos.push_back(new TH2D("escapedHisto","Total Lost Energy vs Number of Escaped Electrons at Implantation Depth Distribution from SRIM",560,0,startingEnergy,100,0,100));

    threadable(-1);
    lostEnergyHistos[0]->Write("lostEnergyHisto", TObject::kSingleKey);
    scatterHistos[0]->Write("scatterHisto", TObject::kSingleKey);
    escapedHistos[0]->Write("escapedHisto", TObject::kSingleKey);

    delete lostEnergyHistos[0];
    delete scatterHistos[0];
    delete escapedHistos[0];
*/
///*
    // printf("Initializing");
    vector<thread> threads;
    for(int depth = 0;depth<2;depth++){
        lostEnergyHistos.push_back(new TH1D(("lostEnergyHisto"+to_string(depth)+"nm").c_str(),("Auger Escape Energy at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy));
        scatterHistos.push_back(new TH2D(("scatterHisto"+to_string(depth)+"nm").c_str(),("Auger Escape Energy vs Scatter Depth at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy,100,0,100));
        escapedHistos.push_back(new TH2D(("escapedHisto"+to_string(depth)+"nm").c_str(),("Total Lost Energy vs Number of Escaped Electrons at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy,100,0,100));
    }

    for(int depth = 1; depth < maxDepth; depth++){
        for(int j = 0; j < threadsPerDepth; j++){
            while(activeThreads>=maxThreads){
                usleep(1000000);
            }
            activeThreads++;
            threads.push_back(thread(threadable,depth));
        }
    }
    for (std::thread & th : threads) if (th.joinable()) th.join();

    for(int depth = 0;depth<maxDepth;depth++){
        lostEnergyHistos[depth]->Write(("lostEnergyHisto"+to_string(depth)+"nm").c_str(), TObject::kSingleKey);
        scatterHistos[depth]->Write(("scatterHisto"+to_string(depth)+"nm").c_str(), TObject::kSingleKey);
        escapedHistos[depth]->Write(("escapedHisto"+to_string(depth)+"nm").c_str(), TObject::kSingleKey);
        delete lostEnergyHistos[depth];
        delete scatterHistos[depth];
        delete escapedHistos[depth];
    }
//*/

    f->Write();
    f->Close();
    delete f;

    return 0;
}
