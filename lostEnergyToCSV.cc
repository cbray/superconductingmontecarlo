string filename = "out-depthVary";

TFile *f = new TFile((filename+".root").c_str(),"READ");

vector<vector<double>> csvData;

vector<double> temp;

// TH1D* h = (TH1D*)f->Get(("lostEnergyHisto"+to_string(0)+"nm").c_str());
TH1D* h = (TH1D*)f->Get("t19_lostEnergyHisto");
// TH1D* h = (TH1D*)f->Get("t8_lostEnergyHisto");
int i=1;
while(h->GetBinLowEdge(i)<h->GetXaxis()->GetXmax()) temp.push_back(h->GetBinLowEdge(i++));

csvData.push_back(temp);
int numBins = temp.size();

/*
for(int i=0;i<84;i++){
    h = (TH1D*)f->Get(("lostEnergyHisto"+to_string(i)+"nm").c_str());
    temp.clear();
    for(int j=1;j<=numBins;j++) temp.push_back(h->GetBinContent(j));
    csvData.push_back(temp);
}
*/

temp.clear();
for(int j=1;j<=numBins;j++) temp.push_back(h->GetBinContent(j));
csvData.push_back(temp);

ofstream fout((filename+".csv").c_str());
for(int i=0;i<csvData[0].size();i++){
    for(int j=0;j<csvData.size();j++){
        fout << csvData[j][i] << ",";
    }
    fout << "\n";
}
