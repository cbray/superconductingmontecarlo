#include <vector>
#include <thread>
#include <iostream>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <utility> // For pair
#include <map>
#include <sys/stat.h>
#include <unistd.h>
#include <mutex>
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "SystemOfUnits.h"
#include "PhysicalConstants.h"
#include "ThreeVector.h"
#include <random>

using namespace std;
using namespace CLHEP;

vector<TH2D*> lostEnergyHistos; // x is the total energy, y is the number of escapes
vector<TH1D*> quasiparticleEnergy; // Final energy that is in quasiparticles (not including escapes)
vector<TH1D*> quasiparticleNumber; // Final number of quasiparticles (not including escapes)
vector<TH1D*> subGapPhononEnergy; // Final energy that is in phonons, which are necesarily sub-gap otherwise they would end up creating more qp
vector<TH1D*> subGapPhononNumber; // Final number of sub-gap phonons


/// Other things
const double fs = 1E-15 * second;
const double electron_mass = electron_mass_c2/c_squared;
double bindingEnergy = 0.00072 * eV;

inline bool file_exists (const std::string& name) { // Credit to stackoverflow user PherricOxide
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

int main(){

    cout << "Select material: Ta (1), Nb (2), Al (3), Hf (4)" << endl;
    int q; cin >> q;
    if (q == 1) {
        bindingEnergy = 0.00072 * eV;
    } else if (q == 2) {
        bindingEnergy = 0.00155 * eV;
    } else if (q == 3) {
        bindingEnergy = 0.00017 * eV;
    } else if (q == 4) {
        bindingEnergy = 0.000021 * eV;
    } else {
        cout << "Out of Bounds. Aborting." << endl;
        return 1;
    }

    vector<ifstream> dataFiles;

    int maxThreads = 1;
    while(file_exists("exitStatisticsOutputFile_t"+to_string(maxThreads)+".out")){
        dataFiles.push_back(ifstream("exitStatisticsOutputFile_t"+to_string(maxThreads)+".out", ios::binary));
        maxThreads++;
    }

    cout << maxThreads << " theads found" << endl;

    if(file_exists("out.root")){
        cerr << "File out.root exists. Aborting" << endl;
        return -3;
    }
    TFile *f = new TFile("out.root","RECREATE");

    for(int i=0;i<dataFiles.size()+1;i++){
        lostEnergyHistos.push_back(new TH2D(("t"+to_string(i)+"_lostEnergyHisto").c_str(),"Total Lost Energy vs Number of Escaped Electrons",2770,0,277,100,0,100));
        lostEnergyHistos[i]->GetXaxis()->SetTitle("Total Lost Energy [eV]");
        lostEnergyHistos[i]->GetYaxis()->SetTitle("Number of Escaped Electrons");
        quasiparticleEnergy.push_back(new TH1D(("t"+to_string(i)+"_quasiparticleEnergy").c_str(),"Final Total Quasiparticle Energy",10000,0,277));
        quasiparticleEnergy[i]->GetXaxis()->SetTitle("Quasiparticle Total Energy [eV]");
        quasiparticleEnergy[i]->GetYaxis()->SetTitle("Counts");
        quasiparticleNumber.push_back(new TH1D(("t"+to_string(i)+"_quasiparticleNumber").c_str(),"Final Number of Quasiparticles",2000000,0,2000000)); //Estimate of how many particles we will have, may need to be updated));
        quasiparticleNumber[i]->GetXaxis()->SetTitle("Quasiparticle Number");
        quasiparticleNumber[i]->GetYaxis()->SetTitle("Counts");
        subGapPhononEnergy.push_back(new TH1D(("t"+to_string(i)+"_subGapPhononEnergy").c_str(),"Final Total Sub-Gap Phonon Energy",10000,0,277));
        subGapPhononEnergy[i]->GetXaxis()->SetTitle("Sub-Gap Phonon Total Energy [eV]");
        subGapPhononEnergy[i]->GetYaxis()->SetTitle("Counts");
        subGapPhononNumber.push_back(new TH1D(("t"+to_string(i)+"_subGapPhononNumber").c_str(),"Final Number of Sub-Gap Phonons",2000000,0,2000000));
        subGapPhononNumber[i]->GetXaxis()->SetTitle("Sub-Gap Phonon Number");
        subGapPhononNumber[i]->GetYaxis()->SetTitle("Counts");
    }

    for(int i=0; i<dataFiles.size(); i++){
        vector<double> exitStatistics = {0,0,0,0,0,0};
        int totalEscapes = 0; int totalEvents = 0; int totalEscapeEvents = 0;
        while(!dataFiles[i].eof()){
            dataFiles[i].read((char*)(&exitStatistics[0]), 6 * sizeof(double));
            // for(int j = 0; j < 6; j++) cout << exitStatistics[j] << ", ";
            // cout << endl;
            lostEnergyHistos[i]->Fill(exitStatistics[0]/eV,exitStatistics[1]);
            quasiparticleEnergy[i]->Fill(exitStatistics[2]/eV);
            quasiparticleNumber[i]->Fill(exitStatistics[3]);
            subGapPhononEnergy[i]->Fill(exitStatistics[4]/eV);
            subGapPhononNumber[i]->Fill(exitStatistics[5]);
            totalEvents++;
            totalEscapes+=exitStatistics[1];
            if(exitStatistics[1]) totalEscapeEvents++;
        }
        double mean = quasiparticleNumber[i]->GetMean();
        double std = quasiparticleNumber[i]->GetStdDev();
        double stdEr = quasiparticleNumber[i]->GetStdDevError();
        cout << "i=" << i << ", epsilon:" << ((/*i*/1)*eV/mean)/bindingEnergy << ", F:" << pow(std,2)/mean << ", pm" << sqrt(pow(std,6) + 4 * pow(mean,2) * pow(std,2) * pow(stdEr,2))/pow(mean,2) << ", avg escapes: " << (double)totalEscapes/(double)totalEvents << ", avg escapes per escape event: " << (double)totalEscapes/(double)totalEscapeEvents << endl;

    }

    for(int i=0;i<lostEnergyHistos.size()-1;i++){ // Create sum histograms at end of list, particularly useful for when you are running the same sim on each thread
        lostEnergyHistos[lostEnergyHistos.size()-1]->Add(lostEnergyHistos[i]);
        quasiparticleEnergy[quasiparticleEnergy.size()-1]->Add(quasiparticleEnergy[i]);
        quasiparticleNumber[quasiparticleNumber.size()-1]->Add(quasiparticleNumber[i]);
        subGapPhononEnergy[subGapPhononEnergy.size()-1]->Add(subGapPhononEnergy[i]);
        subGapPhononNumber[subGapPhononNumber.size()-1]->Add(subGapPhononNumber[i]);
    }
    double mean = quasiparticleNumber[lostEnergyHistos.size()-1]->GetMean();
    double std = quasiparticleNumber[lostEnergyHistos.size()-1]->GetStdDev();
    double stdEr = quasiparticleNumber[lostEnergyHistos.size()-1]->GetStdDevError();
    cout << "i=" << lostEnergyHistos.size()-1 << ", epsilon:" << ((/*i*/1)*eV/mean)/bindingEnergy << ", F:" << pow(std,2)/mean << ", pm" << sqrt(pow(std,6) + 4 * pow(mean,2) * pow(std,2) * pow(stdEr,2))/pow(mean,2) << endl;

    cout << "Extract lost energy histo to csv? (Enter int for thread number, or -1 to cancel)" << endl;
    int thread; cin >> thread;
    while(thread >= 0){
        ofstream fout("t"+to_string(thread)+"_lostEnergyHisto.csv");
        for(int x=0;x<lostEnergyHistos[thread]->GetNbinsX();x++){
            fout << lostEnergyHistos[thread]->GetXaxis()->GetBinLowEdge(x);
            for(int y=0;y<lostEnergyHistos[thread]->GetNbinsY();y++){
                fout << "," << lostEnergyHistos[thread]->GetBinContent(x,y);
            }
            fout << endl;
        }
        fout.close();
        cout << "Extract lost energy histo to csv? (Enter int for thread number, or -1 to cancel)" << endl;
        cin >> thread;
    }

    for(int i=0;i<lostEnergyHistos.size();i++){
        lostEnergyHistos[i]->Write(lostEnergyHistos[i]->GetName(), TObject::kSingleKey);
        quasiparticleEnergy[i]->Write(quasiparticleEnergy[i]->GetName(), TObject::kSingleKey);
        quasiparticleNumber[i]->Write(quasiparticleNumber[i]->GetName(), TObject::kSingleKey);
        subGapPhononEnergy[i]->Write(subGapPhononEnergy[i]->GetName(), TObject::kSingleKey);
        subGapPhononNumber[i]->Write(subGapPhononNumber[i]->GetName(), TObject::kSingleKey);
        delete lostEnergyHistos[i];
        delete quasiparticleEnergy[i];
        delete quasiparticleNumber[i];
        delete subGapPhononEnergy[i];
        delete subGapPhononNumber[i];
        if(i!=lostEnergyHistos.size()-1) dataFiles[i].close();
    }
    f->Write();
    f->Close();
    delete f;

    return 0;
}
