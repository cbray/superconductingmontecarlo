#include <vector>
#include <algorithm>
#include <list>
#include <thread>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <utility> // For pair
#include <map>
#include <sys/stat.h>
#include <unistd.h>
#include <mutex>
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"
// #include "CLHEP/Vector/ThreeVector.h"
#include <random>

using namespace std;
using namespace CLHEP;
// using namespace std::chrono;


/// Run Properties
const int numIterationsPerThread = 1000;
int activeThreads = 0;
const int maxThreads = 1;
const double startingEnergy = 56*eV;
// SRIM depth distributions, for when depth isnt specified
// TF1 depthFunction("depthFunction","((x/553)**(0.829))*(exp(-(x/553)**1.829))",0,1660); // Shallow Implant
TF1 depthFunction("depthFunction","((x/774.8)**(0.996))*(exp(-(x/774.8)**1.996))",0,1660); // Shallow Implant V2
// TF1 depthFunction("depthFunction","((x/1220)**(0.829))*(exp(-(x/1220)**1.829))",0,1660); // Deep Implant
const double initialDepth = -1; //82.5; // -1 to use depth function, 0 or above to use set depth in nanometers
const double positionReadoutEnergy = -1; // 4.5 * eV /* chosen as work function to measure escape range */; // Quasiparticles below this energy are stopped and their positions recorded. If negative, positions are not recorded. TODO FIXME will be slow to use on many threads
const bool timePositionReachout = 0; // Write out the full time / position for each quasiparticle at every step. WARNING: (TODO FIXME?) will not play nice with multithreading. Also does not play nice with positionReadoutEnergy. Format is ID, time, deltaR^2, x, y, z, where positions are in nm and time is in nanosecond
const bool useStartingPhonons = 0; // If true, converts starting energy into phonons according to a^2F before beginning simulation
const bool gottaGoFast = 0; //If true, will stop quasiparticles below the workfunction to run events more quickly
const bool timeOrderEvents = 1; // If true, will run events fully time-ordered, rather than evaluating each particle to its end before moving to the next. This may be slower depending on the cache size and performance, but some effort has gone into optimizing this


/// Material properties
const double phononVelocity = 3400 * meter/second;
const double halfSideLength = 34 * micrometer; // Origin is in the center of the detector
const double halfHeight = 82.5 * nanometer;
const vector<double> detectorSize = {halfSideLength, halfSideLength, halfHeight}; // For a vectorized quantity
map<int,pair<vector<double>,vector<double>>> phononCouplingTable;
const double minIonizationEnergy = 0.033 * eV; // Below this energy, no ioniztion (direct cooper pair breaking) occurs, only slowdown by phonon creation

/// Other things
const double fs = 1E-15 * second;
const double electron_mass = electron_mass_c2/c_squared;

// Material Constants
// Ta
const string cdfTabFilename = "CDFtabTa.csv";
const double bindingEnergy = 0.00072 * eV; // Ta
const double couplingTableDelta = bindingEnergy/100;
const int maxCouplingTableIndex = 3471; // Ta
const double fermiVelocity = 3.6e5 * meter/second; // From Stephan Friedrich Dissertation
const double workFunction = 4.5 * eV;
// Al
// const string cdfTabFilename = "CDFtabAl.csv";
// const double bindingEnergy = 0.00017 * eV; // Al
// const double couplingTableDelta = bindingEnergy/100;
// const int maxCouplingTableIndex = 1870; // Nb
// const double fermiVelocity = 2.2e6 * meter/second; // From Stephan Friedrich Dissertation, for tantalum.  for Aluminum
// Nb
// const string cdfTabFilename = "CDFtabNb.csv";
// const double bindingEnergy = 0.00155 * eV; // Nb
// const double couplingTableDelta = bindingEnergy/100;
// const int maxCouplingTableIndex = 25882; // Al
// const double fermiVelocity = 3.6e5 * meter/second; // From Stephan Friedrich Dissertation, for tantalum. 2.2e6 for Aluminum
// Hf
// const string cdfTabFilename = "CDFtabHf.csv";
// const double bindingEnergy = 0.000021 * eV; // Hf
// const double couplingTableDelta = bindingEnergy/20; // Hf
// const int maxCouplingTableIndex = 25654; // Hf
// const double fermiVelocity = 3.6e5 * meter/second; // From Stephan Friedrich Dissertation, for tantalum. 2.2e6 for Aluminum
// Au
// const double fermiVelocity = 1.40e6 * meter/second; // From hyperphysics
// const double workFunction = 5.1 * eV; // hyperphysics - gold
// const double workFunction = 5.73 * eV; // HgTe https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.33.1088 calculated photoelectric threshold = 0.96 φexp


/// Outputs
// vector<TH2D*> lostEnergyHistos; // x is the total energy, y is the number of escapes
// vector<TH1D*> quasiparticleEnergy; // Final energy that is in quasiparticles (not including escapes)
// vector<TH1D*> quasiparticleNumber; // Final number of quasiparticles (not including escapes)
// vector<TH1D*> subGapPhononEnergy; // Final energy that is in phonons, which are necesarily sub-gap otherwise they would end up creating more qp
// vector<TH1D*> subGapPhononNumber; // Final number of sub-gap phonons
ofstream outputPositionFile;
mutex outputPositionMutex;
mutex fuckOffRoot; // To read the TF1 thread-safely. Cause aparantly that causes issues now...


/// Generic functions

// Get seed for random number generators
// Credit to posop on stackoverflow for the implementation
template<typename T>
T random_seed(bool uRandom=1){
    T seed;
    std::ifstream file(uRandom?"/dev/urandom":"/dev/random",std::ios::binary);
    if(file.is_open()){
        char *memblock;
        size_t size=sizeof(T);
        memblock=new char [size];
        file.read(memblock,size);
        file.close();
        seed=*reinterpret_cast<T*>(memblock);
        delete[] memblock;
        return seed;
    }else{return random_seed<T>();} // Continually retry until /dev/random is free
}

// Move element in vector to a new location
// Credit to Elan Hickler on Stackoverflow for the implementation
template <typename t> void move(std::vector<t>& v, size_t oldIndex, size_t newIndex)
{
    if (oldIndex > newIndex)
        std::rotate(v.rend() - oldIndex - 1, v.rend() - oldIndex, v.rend() - newIndex);
    else
        std::rotate(v.begin() + oldIndex, v.begin() + oldIndex + 1, v.begin() + newIndex + 1);
}

// Get random double [0,1)
std::uniform_real_distribution<double> flat(0.0,1.0);
std::mt19937_64 globalGenerator(random_seed<unsigned int>());


// Determine if a file exists
inline bool file_exists (const std::string& name) { // Credit to stackoverflow user PherricOxide
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

// Get a randomly oriented unit 3-vector
vector<double> randomUnitVector(std::mt19937_64& gen = globalGenerator){
    double theta = 2 * M_PI * flat(gen);
    double u = 1 - 2 * flat(gen);
    return {sqrt(1-u*u)*cos(theta),sqrt(1-u*u)*sin(theta),u};
}

// Return the cross product of two 3-vectors
vector<double> crossProduct(const vector<double>& a, const vector<double>& b){
    return {a[1]*b[2] - a[2]*b[1], a[2]*b[0]-a[0]*b[2], a[0]*b[1]-a[1]*b[0]};
}

// Return the norm (unit vector) of a 3-vector
vector<double> norm(const vector<double>& a){
    double scale = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
    return {a[0]/scale, a[1]/scale, a[2]/scale};
}

// Scale a vector to a certain length
vector<double> scaleVector(const vector<double>& a, double newScale){
    double scale = newScale / sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
    return {a[0]*scale, a[1]*scale, a[2]*scale};
}

// Return true if particle is outside volume, false otherwise
bool checkPosition(const vector<double>& position){
    if(abs(position[0]) > halfSideLength) return true;
    if(abs(position[1]) > halfSideLength) return true;
    if(abs(position[2]) > halfHeight) return true;
    return false;
}

// Get random vector moving at the fermiVelocity
vector<double> getRandomStartingVelocity(){
    vector<double> startingDirection = randomUnitVector();
    // startingDirection[2] = 0; // For old gold simulations, this forced the initial particle to be parallel to surface - assumption of initial photon perpendicular to surface
    vector<double> startingVelocity = scaleVector(startingDirection,fermiVelocity);

    return startingVelocity;
}

// Get random vector at the starting position
vector<double> getRandomStartingPosition(int depth = -1, std::mt19937_64& gen = globalGenerator){
    vector<double> startingPosition = {0,0,halfHeight - depth*nanometer};
    if(depth >= 0) return startingPosition;
    do {
        startingPosition[0] = (flat(gen)-0.5) * 50 * micrometer;
        startingPosition[1] = (flat(gen)-0.5) * 50 * micrometer;
    } while(sqrt(startingPosition[0]*startingPosition[0] + startingPosition[1]*startingPosition[1]) > 25*micrometer);

    fuckOffRoot.lock();
    startingPosition[2] = 82.5*nanometer - depthFunction.GetRandom()*angstrom; // Draw from distribution
    fuckOffRoot.unlock();

    // Initial starting position in gold:
    // startingPosition[0] = (flat(gen)-0.5)*halfSideLength*2;
    // startingPosition[1] = (flat(gen)-0.5)*halfSideLength*2;
    // startingPosition[2] = halfHeight - nm*(-36.23216473836594* log(-0.999983951809822* (-1.000016048447727 + flat(gen)))); // 277 eV photon in 400nm gold - calculated these constatns in implantationDepthCalc.nb
    // startingPosition[2] = halfHeight - nm*(-62* log(-0.998422025005843 * (-1.001580468934615 + flat(gen)))); // 277 eV photon in 400nm HgTe (62nm) - calculated these constatns in implantationDepthCalc.nb . Extra constants for making sure that all events happen in the material.

    return startingPosition;

}

// Get the new position of a particle that might bounce off of the edge of the detector (internal reflection)
// Also modifies a velocity you pass to match the reflections.
// TODO: Make diffuse reflection option
void reflectPosition(vector<double>& initialPosition, vector<double> deltaPosition, vector<double> &velocity){
    vector<double> testPosition = {initialPosition[0] + deltaPosition[0], initialPosition[1] + deltaPosition[1], initialPosition[2] + deltaPosition[2]};

    while(checkPosition(testPosition)){ // Wont execute if testPosition is good, so we can skip everything if the initial test is good, or account for multiple bounces (eg very long interaction length)
        for(int i=0;i<3;i++){ // Repeat for each axis
            if(testPosition[i] > detectorSize[i]){
                double delta = detectorSize[i] - initialPosition[i];
                // cout << delta << endl;
                deltaPosition[i] = -(deltaPosition[i]-delta); // Subtract the distance already traveled in that direction, and then reflect the velocity
                initialPosition[i] = detectorSize[i];
                testPosition[i] = initialPosition[i] + fmod(deltaPosition[i],4*detectorSize[i]);
                velocity[i] *= -1;
            }
            if(testPosition[i] < -detectorSize[i]){ // Opposite side for reflection
                double delta = -detectorSize[i] - initialPosition[i];
                // cout << delta << endl;
                deltaPosition[i] = -(deltaPosition[i]-delta); // Subtract the distance already traveled in that direction, and then reflect the velocity
                initialPosition[i] = -detectorSize[i];
                testPosition[i] = initialPosition[i] + fmod(deltaPosition[i],4*detectorSize[i]);
                velocity[i] *= -1;
            }
        }

        // cout << "bounce!" << endl;

    }

    initialPosition = testPosition;
}

bool readInPhononCouplingTable(){
    ifstream fin(cdfTabFilename); // This is a global, though it should probably not be
    if(!fin.good()) return false;
    cout << "Reading in CDF table from " << cdfTabFilename << endl;
    while(!fin.eof()){
        string temp;
        getline(fin,temp);
        if(temp.length() < 2) continue;
        // cout << temp << endl;
        stringstream ss(temp);
        int qpEnergy;
        ss >> qpEnergy;
        vector<double> lookupValue;
        vector<double> phononEnergy;
        double lookup,phonon;
        while(ss >> lookup >> phonon) {
            lookupValue.push_back(lookup);
            phononEnergy.push_back(eV*phonon);
            // cout << lookup << ";" << phonon << endl;
        }
        phononCouplingTable[qpEnergy] = {lookupValue,phononEnergy};
        // cout << phononCouplingTable[qpEnergy].first.size() <<  "," << phononCouplingTable[qpEnergy].second.size() << endl;
    }
    // cout << phononCouplingTable.size() << endl;
    cout << "Finished reading in CDF table." << endl;
    return true;
}


/// particle, quasiparticle, and phonon particle definitions
class particle {
    public:
        double mass;
        int ID;
        int parentID;
        vector<double> position;
        vector<double> velocity;
        double energy;
        double time;

        double nextTime;
        int nextInteractionType;


        virtual double momentum(){};

        virtual int process(map<double,particle*> &particleTree /*passing the particle tree so any new particles can be appended. Index = ID*/, vector<double> &exitStatistics, std::mt19937_64& gen, int& nextID) = 0;
        // This function will do a single iteration of that particle moving through the material.
        // First, for each process on the particle, it will generate interaction lengths (according to their distributions, with randomness in there), and use the shortest of each process to determine which process happens
        // It will then create that process, updating the position and velocity of this particle, and add any daughter particles to the end of particleTree
            // Make sure to check here that the particle travelling ballistically for that distance does not leave the material left. If it does, do the ending things for escape if it has more energy than the workfunction, otherwise have it reflect
        // If the particle is less than its threshold energy, it will add its statistics to exitStatistics, which is a vector of all the output data (escape energy (add energy if escaped), number of escaped particles (increment if escaped), quasiparticle total (add energy if non-escape qp), quasiparticle number (increment if non-escape qp), subGapPhononEnergy (add energy if sub-gap phonon), subGapPhononNumber (increment if sub-gap phonon) ). This vector is then used to increment the output histograms after each event
        // If the particle is less than the threshold energy, then it will return 0, so that the thread will know to move to the next particle
        // Otherwise, it will return 1 (or maybe other values if there are eventually other things we want to look at) to let the thread know that it should do another even

        virtual ~particle(){};

        // Time sorting operator to allow sorting of the particle tree in time
        bool operator <= (const particle& other) const {
            return (nextTime <= other.nextTime);
        }
};

// Implement sorting of pointers. Not as clean as I'd hoped for use in std::sort but good enough
inline bool compareParticlePointers(particle* a, particle* b) { return (*a <= *b); }

// TODO SPEED THIS UP. Slowest part of entire simulation by at least two orders of magnitude and needs to be run for each event
void placeSorted(std::map<double,particle*> &particleTree, particle* newParticle){
    // auto begin = std::chrono::high_resolution_clock::now();

    while(particleTree.find(newParticle->nextTime) != particleTree.end()) {
        newParticle->nextTime = nextafter(newParticle->nextTime,DBL_MAX);
    }

    // auto end = std::chrono::high_resolution_clock::now();
    // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns finding" << std::endl;

    particleTree[newParticle->nextTime] = newParticle;

    // begin = std::chrono::high_resolution_clock::now();
    // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(begin-end).count() << "ns placing" << std::endl;

    return;

    // auto begin = std::chrono::high_resolution_clock::now();
    // for (auto it = particleTree.begin(); it != particleTree.end(); ++it){
        // if(newParticle->nextTime <= (*it)->nextTime){
            // auto end = std::chrono::high_resolution_clock::now();
            // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns finding" << std::endl;
            // particleTree.insert(it,newParticle);
            // begin = std::chrono::high_resolution_clock::now();
            // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(begin-end).count() << "ns placing" << std::endl;
            // return;
        // }
    // }
    // Just in case
    // particleTree.push_back(newParticle);
    // return;
}

class phonon : public virtual particle {
    public:

        double real_tau;

        phonon(int id, int parentid, vector<double> Position, vector<double> Velocity, double Energy, double Time) {
            mass = 0; ID = id; parentID = parentid; velocity = Velocity; energy = Energy;
            position = Position;
            time = Time;
            nextTime = -1;
            nextInteractionType = -1;
            real_tau = -1;
        }
        double momentum(){
            return energy / phononVelocity;
        }

        const double arbitraryScaleFactorPhQP = 1;

        int process(map<double,particle*> &particleTree, vector<double> &exitStatistics, std::mt19937_64& gen, int& nextID); // Need to declare this function after quasiparticle class definition because qp is referenced in process
};

class quasiparticle : public virtual particle {
    public:
        double real_tau;
        double freePath;

        quasiparticle(int id, int parentid, vector<double> Position, vector<double> Velocity, double Energy, double Time) {
            mass = electron_mass; ID = id; parentID = parentid; velocity = Velocity; energy = Energy;
            position = Position;
            time = Time;
            nextTime = -1;
            nextInteractionType = -1;
            freePath = -1;
            real_tau = -1;
        }

        double vSquared(){
            return (velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2]);
        }
        double momentum(){
            return mass * sqrt(vSquared());
        }

        // qp - qp interaction parameters
        const double mfpA = 9;
        const double mfpB = -6.61;
        const double mfpE_th = 0; // E_th = E_F
        const double mfpE_0 = 1; // 1 eV
        const double mfpa = 0.001;
        const double mfpb = 1.8;
        const double arbitraryScaleFactorQPQP = 1;
        // Gold/HgTe Z ratio is 1.19697 (Au=79,HgTe avg is 66 (80 and 52 respectively)). Density ratio is 2.42 (19.3 g/cm3 for Au, 7.965 g/cm3 for HgTe)

        // qp - phonon interaction parameters
        const double arbitraryScaleFactorQPPh = 1;

        // Extra physics that I might consider as an easy extension:
        // Recomination
        // Trapping (probabalistically)
        // Tunneling (probabalistically upon interaction at lower boundary)
        // Hot Spots (Zhender QP relax 1995 paper)

        int process(map<double,particle*> &particleTree /*passing the particle tree so any new particles can be appended. Index = ID*/, vector<double> &exitStatistics, std::mt19937_64& gen, int& nextID){
            // Writeout time/position if requested
            if(timePositionReachout){
                outputPositionMutex.lock();
                outputPositionFile << ID << ", " << time/nanosecond << ", " << (position[0]*position[0] + position[1]*position[1] + position[2]*position[2])/nm << ", " << position[0]/nm << ", " << position[1]/nm << ", " << position[2]/nm << endl;
                outputPositionMutex.unlock();
            }

            if(energy < positionReadoutEnergy){ // Stop quasiparticle at higher energy for position readout TODO FIXME will be slow to use on many threads
                exitStatistics[2] += energy;
                exitStatistics[3]++;

                outputPositionMutex.lock();
                outputPositionFile << sqrt(position[0]*position[0] + position[1]*position[1] + position[2]*position[2])/nm << "; " << energy/eV << "; " << position[0]/nm << ", " << position[1]/nm << ", " << position[2]/nm << endl;
                outputPositionMutex.unlock();

                return -1; // Code to end simulatiaon after first particle
            }

            if(energy < 3*bindingEnergy){
                exitStatistics[2] += energy;
                exitStatistics[3]++;
                // cout << "Stop QP - low energy" << endl;
                return 0; // End particle
            }

            if(gottaGoFast && energy < workFunction){
                exitStatistics[2] += energy;
                exitStatistics[3]++;
                return 0; // End particle below workfunction if gottaGoFast
            }

            if(nextInteractionType == 1){
                // PHYSICS: Direct ionization
                // Assumptions:
                    // Scattered electron is "free" and the hole it leaves behind is neglected
                    // Coulomb scattering happens, with an angle determined by a rutherford-esque scattering w/ a flat generated b within [0,1]nm
                    // Mean free path is derived from the mean free path of electrons in silver, Ziaja paper

                // double meanFreePath = angstrom * (sqrt(energy/eV)/(mfpa * pow((energy/eV) - mfpE_th,mfpb))  +  ((energy/eV) - (mfpE_0 * exp(-mfpB/mfpA))) / (mfpA * log((energy/eV)/mfpE_0) + mfpB));
                // double freePath = -log(flat()) * meanFreePath * arbitraryScaleFactorQPQP; // Exponential distribution with mean of meanFreePath
                double scale = freePath * mass / momentum(); // Removing the magnitude of velocity, so that the delta position is the length of freePath
                vector<double> deltaPosition = {velocity[0] * scale, velocity[1] * scale, velocity[2] * scale};
                vector<double> finalPosition = {position[0] + deltaPosition[0], position[1] + deltaPosition[1], position[2] + deltaPosition[2]};

                // Check the interaction length. If the particle has more than the workFunction in energy and will escape before it interacts, then record that information and return 0
                if(energy > workFunction &&  checkPosition(finalPosition)){
                    exitStatistics[0] += energy;
                    exitStatistics[1]++;
                    // cout << "Stop QP - escape: "+to_string(energy/eV)+" eV\n";
                    return 0; // End particle
                }

                reflectPosition(position,deltaPosition,velocity);

                // Get axes
                vector<double> perpendicular = norm(crossProduct(velocity,randomUnitVector(gen)));
                vector<double> parallel = norm(velocity);

                // Kinematics and particle creation
                double cosTheta = -1;
                double unitlessInitialEnergy = energy/eV;
                while(cosTheta < 0) {
                    double b = flat(gen);
                    cosTheta  = 1 + (1/(-0.5 - (0.24113841650414078406222985374214033585 *b*b*unitlessInitialEnergy*unitlessInitialEnergy )));
                }
                double newEnergy = energy * cosTheta * cosTheta;
                double daughterEnergy = energy - newEnergy;
                energy = newEnergy;


                velocity = scaleVector(randomUnitVector(gen),fermiVelocity); // FIXME (Drude-Like randomization)
                vector<double> daughterVelocity = scaleVector(randomUnitVector(gen),fermiVelocity); // FIXME (Drude-Like randomization)
                double deltaTime = freePath / fermiVelocity;

                time += deltaTime;
                quasiparticle *daughter1 = new quasiparticle(nextID++, ID, position, daughterVelocity, daughterEnergy, time);

                // TEST
                // cout << "TEST: new energy: " << energy/eV << ", daughter energy: " << daughter1->energy/eV << ", initialEnergy: " << initialEnergy/eV << ", totalFinalEnergy: " << (energy+daughter1->energy+daughter2->energy)/eV << endl;

                // particleTree.push_front(daughter1);
                int processCode = daughter1->process(particleTree,exitStatistics,gen,nextID); // Generate the correct next event time for the new particle
                if(processCode == -1) {
                    // cerr << "Daughter requested ending simulation" << endl;
                    return -1;
                } else if (processCode == 0) {
                    // cout << "Daughter requested to be immediately terminated" << endl;
                    delete daughter1;
                } else {
                    placeSorted(particleTree,daughter1);
                }
            } else if(nextInteractionType == 2){
                // PHYSICS: Phonon creation with partial energy deposition
                // Assumptions:
                    // Phonon is created by selecting from the allowed phonon energies according to their contribution to the inverse lifetime, as if they are each different decay channels
                    // Mean time is determined from kurakado1982 and kaplan1976
                    // Assume phonon always moves at speed of sound zehnder_QP_relax_1995.pdf page 3

                // double tau_qp = 2.50191E-19 * second / pow(energy/eV + 0.000138875,3);
                // double real_tau = - log(flat()) * tau_qp * arbitraryScaleFactorQPPh;
                reflectPosition(position,{velocity[0] * real_tau, velocity[1] * real_tau, velocity[2] * real_tau},velocity);
                double initialEnergy = energy;

                // Kinematics and particle creation
                int qpEnergy = ceil(energy/couplingTableDelta); qpEnergy = (qpEnergy>maxCouplingTableIndex)?maxCouplingTableIndex:qpEnergy; // Maximum limit on the coupling table, higher energies correspond to the same distribution because the higher energy coupling goes to 0. Max is 3471 for Ta, 1870 for Nb, 25882 for Al, 25654 for Hf
                // cout << "qpEnergy: " << qpEnergy << endl;
                double phononEnergy = 0; // int q = 0;
                do{
                    double x0 = flat(gen);
                    double lowerIndex = lower_bound(phononCouplingTable[qpEnergy].first.begin(),phononCouplingTable[qpEnergy].first.end(),x0) - phononCouplingTable[qpEnergy].first.begin();
                    if(lowerIndex == 0){
                        phononEnergy = phononCouplingTable[qpEnergy].second[lowerIndex];
                    } else { // Perform linear interpolation if we are not looking at exactly the first
                        double x1 = phononCouplingTable[qpEnergy].first[lowerIndex-1];
                        double x2 = phononCouplingTable[qpEnergy].first[lowerIndex];
                        double y1 = phononCouplingTable[qpEnergy].second[lowerIndex-1];
                        double y2 = phononCouplingTable[qpEnergy].second[lowerIndex];
                        phononEnergy = ((y2-y1)/(x2-x1)) * (x0-x1) + y1; // Find the value in the CDF phononCouplingTable for the given qp energy that corresponds to the randomly generated value, and use that to determine the weighted phonon energy.
                        // q++;
                    }
                } while(energy - phononEnergy < bindingEnergy); // Ensure we do not take too much energy away from the qp
                // cout << "phononEnergy: " << phononEnergy/eV << " eV, new qp energy: " << (energy-phononEnergy)/eV << " eV, q: " << q << endl;
                energy -= phononEnergy;

                // Randomize directions - drude
                vector<double> phV = scaleVector(randomUnitVector(gen),phononVelocity);
                velocity = scaleVector(randomUnitVector(gen),fermiVelocity);
                time += real_tau;

                // Create phonon going at the correct velocity and with the new energy
                phonon* daughter = new phonon(nextID++,ID,position,phV,phononEnergy,time);
                // particleTree.push_front(daughter);
                int processCode = daughter->process(particleTree,exitStatistics,gen,nextID); // Generate the correct next event time for the new particle
                if(processCode == -1) {
                    // cerr << "Daughter requested ending simulation" << endl;
                    return -1;
                } else if (processCode == 0) {
                    // cout << "Daughter requested to be immediately terminated" << endl;
                    delete daughter;
                } else {
                    placeSorted(particleTree,daughter);
                }

                // TEST
                // cout << "Initial energy: " << initialEnergy/eV << ", new energy: " << energy/eV << ", phonon energy: " << phononEnergy/eV << ", total final energy: " << (energy+phononEnergy)/eV << endl;
            } else if(nextInteractionType != -1){
                cerr << "Unexpected nextInteractionType, big problems." << endl;
                return -1;
            }

            // Generate next event type and time
            double tau_qp = 2.50191E-19 * second / pow(energy/eV + 0.000138875,3); // Calculate tau for qp slowdown (qp -> qp + ph)
            real_tau = - log(flat(gen)) * tau_qp * arbitraryScaleFactorQPPh;

            double meanFreePath = angstrom * (sqrt(energy/eV)/(mfpa * pow((energy/eV) - mfpE_th,mfpb))  +  ((energy/eV) - (mfpE_0 * exp(-mfpB/mfpA))) / (mfpA * log((energy/eV)/mfpE_0) + mfpB)); // Calculate tau for qp direct cooper pair breaking (qp -> 3qp)

            // double inputE = log(energy/eV);
            // double meanFreePath = angstrom * exp(31.470340298261927 * pow(inputE,1) - 45.758320439642276 * pow(inputE,2) + 30.440155340029477 * pow(inputE,3) - 11.506059530964565 * pow(inputE,4) + 2.654270941103958 * pow(inputE,5) - 0.3805672217233523 * pow(inputE,6) + 0.033125278283988244 * pow(inputE,7) - 0.0016037214140757722 * pow(inputE,8) + 0.00003314889171144545 * pow(inputE,9) ); // Gold fit. Rough and dirty. https://doi.org/10.1016/j.elspec.2019.02.003
            // cout << to_string(meanFreePath/nm)+"\n";


            freePath = -log(flat(gen)) * meanFreePath * arbitraryScaleFactorQPQP; // Exponential distribution with mean of meanFreePath

            // cout << real_tau * fermiVelocity / nm << ", " << freePath/nm << endl;

            // Doing the decision between the direct cooper pair breaking process and the phonon creation process by energy
            // I was going to do it by interaction lengths but the formulation I have for that right now would have the interactions switching over at an energy that disagrees with the values found in kurakado1982
            // Assumptions:
                // Approximation of hard switchover at qp energy = 0.033 eV so that it cannot create a phonon with too high energy
            // TODO: Switch to better method
            // if(energy > 0.033 * eV){
            if((gottaGoFast || (real_tau * fermiVelocity > freePath)) && (energy > minIonizationEnergy)){
                nextInteractionType = 1;
                double deltaTime = freePath / fermiVelocity;
                nextTime = time + deltaTime;
            } else {
                nextInteractionType = 2;
                nextTime = time + real_tau;
            }
            // cout << "Quasiparticle " << ID << " time process." << endl;
            return 1;
        }
};


    // PHYSICS: Phonon complete absorption by cooper pair breaking and creation of 2 quasiparticles
    // Assumptions:
        // Phonon deposits its entire energy into the created qp - the phonon is killed afterward
        // Resulting qp cannot mathematically carry same energy & momentum as the initial phonon. Instead, they will carry the same energy as the initial phonon, and travel in the same direction.
        // Resulting qp move in the same initial direction. (semiclassical description of cooper pairs as localised particles)
int phonon::process(map<double,particle*> &particleTree /*passing the particle tree so any new particles can be appended. Index = ID*/, vector<double> &exitStatistics, std::mt19937_64& gen, int& nextID){
    // If the phonon is sub-gap in energy, then increment the exit statistics and kill the particle
    if(energy < 2*bindingEnergy){
        exitStatistics[4] += energy;
        exitStatistics[5]++;
        // cout << "Stop Ph - low energy" << endl;
        return 0; // End particle
    }

    // If the phonon has been initialized, process it
    if(nextInteractionType == 1){
        reflectPosition(position,{velocity[0] * real_tau, velocity[1] * real_tau, velocity[2] * real_tau},velocity);
        // double newQPEnergy = flat(gen) * (energy - bindingEnergy) + bindingEnergy; // Generate random first qp energy in acceptable range
        // Use more accurate distribution according to probability from kaplan integrand, including the edge non-linearities. Fit to in mathematica due to lack of analytical integral, errors should be less than 0.1% for cases betweek 2delta and 100 delta.
        double omegaFraction = energy/bindingEnergy;
        double select = flat(gen); select = (select < 1e-10)?1e-10:select; // Loss of numerical accuracy at low select values causes nan/inf values to propogate.
        double newQPEnergy = bindingEnergy*(1+((omegaFraction-2)/(1+pow((1-select)/select, (1+ (327188.3469929788 * (omegaFraction-1) * exp(-12.536066044594444 * pow(omegaFraction-0.41008838128465896,0.11381081111817092) )) ) ))));

        vector<double> newQPVelocity = scaleVector(velocity,fermiVelocity); // qp move in the direction of the ph.

        time += real_tau;

        quasiparticle *daughter1 = new quasiparticle(nextID++, ID, position, newQPVelocity, newQPEnergy, time);
        quasiparticle *daughter2 = new quasiparticle(nextID++, ID, position, newQPVelocity, energy-newQPEnergy, time);

        // TEST
        // cout << "TEST: newQPEnergy: " << newQPEnergy/eV << ", daughter energy: " << daughter1->energy/eV << endl;

        // particleTree.push_front(daughter1);
        // particleTree.push_front(daughter2);
        int processCode = daughter1->process(particleTree,exitStatistics,gen, nextID); // Generate the correct next event time for the new particle
        if(processCode == -1) {
            // cerr << "Daughter 1 requested ending simulation" << endl;
            return -1;
        } else if (processCode == 0) {
            // cout << "Daughter 1 requested to be immediately terminated" << endl;
            delete daughter1;
        } else {
            placeSorted(particleTree,daughter1);
        }
        processCode = daughter2->process(particleTree,exitStatistics,gen, nextID); // Generate the correct next event time for the new particle
        if(processCode == -1) {
            // cerr << "Daughter 2 requested ending simulation" << endl;
            return -1;
        } else if (processCode == 0) {
            // cout << "Daughter 2 requested to be immediately terminated" << endl;
            delete daughter2;
        } else {
            placeSorted(particleTree,daughter2);
        }

        return 0; // End particle
    } else if(nextInteractionType != -1){
        cerr << "This should never happen. Big problems." << endl;
        return -1;
    }
    // If the particle has not been initialized or just , then initialize it and determine its next step position. Since the phonon only takes a single step in the simulation, there is no need for recalculating these values after each step. but the code is setup to implement a 'transport' process or something similar
    double tau_phonon = (-6.71875 + (1.19206 / (0.00120873 + energy/eV /*Energy input in eV*/))) * fs/*output is in fs*/;  // Fit to tau_phonon from kurakado1982, kaplan1976 that I calculated for our case
    real_tau = - log(flat(gen)) * tau_phonon * arbitraryScaleFactorPhQP;

    time = nextTime;
    nextTime = time + real_tau;
    nextInteractionType = 1;
    // cout << "Phonon " << ID << " time process." << endl;
    return 1; // Return the particle to the list for sorting
}




/// Simulation structure
void runEvent(particle* initialParticle, int histogramID, ofstream& exitStatisticsOutputFile, std::mt19937_64& gen){
    map<double,particle*> particleTree;
    if(!useStartingPhonons){
        // particleTree.push_front(initialParticle);
        placeSorted(particleTree,initialParticle);
    } else {
        double energy = initialParticle->energy;
        int i = 0;
        while(energy > 0){
            // int id, int parentid, vector<double> Position, vector<double> Velocity, double Energy, double Time
            double thisEnergy = phononCouplingTable[maxCouplingTableIndex].second[lower_bound(phononCouplingTable[maxCouplingTableIndex].first.begin(),phononCouplingTable[maxCouplingTableIndex].first.end(),flat(gen)) - phononCouplingTable[maxCouplingTableIndex].first.begin()]; // Get phonon energy according to a^2F (highest energy value of CDFtab, at index 3471 for Ta, or 1870 for Nb, or 25882 for Al, or 25654 for Hf)
            thisEnergy = (energy < thisEnergy)?energy:thisEnergy;
            particle* temp = new phonon(i, -1, initialParticle->position, scaleVector(randomUnitVector(),phononVelocity), thisEnergy, 0);
            // particleTree.push_back(temp);
            placeSorted(particleTree,temp);
            energy-=thisEnergy;
            i++;
            // cout << "thisEnergy: " << thisEnergy/eV << ", energy: " << energy/eV << " i: " << i << ", particleTree.size(): " << particleTree.size() << endl;
        }
        cout << "Starting with " << i << " phonons." << endl;
    }

    vector<double> exitStatistics = {0,0,0,0,0,0};
    int nextID = 1;

    // auto begin = std::chrono::high_resolution_clock::now();
    // auto end = std::chrono::high_resolution_clock::now();
    while(particleTree.size()>0){
        // begin = std::chrono::high_resolution_clock::now();
        int processCode = (particleTree.begin()->second)->process(particleTree,exitStatistics, gen, nextID);
        // end = std::chrono::high_resolution_clock::now();
        // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns processing" << std::endl;

        // cout << "processCode: " << processCode << endl;
        // cout << "particleTree.begin()->second->ID: " << particleTree.begin()->second->ID << endl;
        // cout << "Size of particle tree: " << particleTree.size() << endl;

        if(processCode == 0){
            // Particle is done, remove it from the list - fast with lists
            if(particleTree.begin()->second->ID != 0) delete (particleTree.begin()->second); // Cleanup dynamically allocated particles, which is every particle but the first
            particleTree.erase(particleTree.begin());
            // cout << "Erased?" << endl;
        } else if(processCode == -1){
            cerr << "Particle experienced an issue and requested simulation end. Breaking out of process loop." << endl;
            break;
        } else if (timeOrderEvents){ // Particle needs to be placed in its new position in the list
            // begin = std::chrono::high_resolution_clock::now();
            particle* temp= particleTree.begin()->second;
            particleTree.erase(particleTree.begin());
            placeSorted(particleTree,temp);
            // end = std::chrono::high_resolution_clock::now();
            // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns sorting" << std::endl;
        }

        // List sorting no longer needed, as elements are added in order
        // Sort remaining particles in time - new particles are placed at the front to have their first event initialized, and events are then processed in order.
        // sort(particleTree.begin(),particleTree.end(),compareParticlePointers);
    }

    /* old, not-time-ordered event processing
    for(int i=0;!endSim && i<particleTree.size();i++){
        // TODO: Resort the particleTree after each interaction and remove dead particles to keep the head of the vector as the next event - then it shouldnt be necesary to iterate over the list just keep running the first element until no more elements remain

        while(int processCode = particleTree[i]->process(particleTree,exitStatistics, gen)){
            // Might eventually do something with processCode to print additional statistics but rn this will just loop until the particle exits with a 0 code - eg its done
            // cout << "process has stepped with process code " << processCode << endl;

            if(processCode == -1){
                endSim = 1;
                break;
            }
            // cout << "particleTree.size(): " << particleTree.size() << ", current particle: " << i << endl;
        }
        // cout << "process has stopped with process code 0" << endl;
    }
    */

    // lostEnergyHistos[histogramID]->Fill(exitStatistics[0],exitStatistics[1]);
    // quasiparticleEnergy[histogramID]->Fill(exitStatistics[2]);
    // quasiparticleNumber[histogramID]->Fill(exitStatistics[3]);
    // subGapPhononEnergy[histogramID]->Fill(exitStatistics[4]);
    // subGapPhononNumber[histogramID]->Fill(exitStatistics[5]);

    exitStatisticsOutputFile.write(reinterpret_cast<const char*>(&exitStatistics[0]), 6 * sizeof(double));

    // cout << "Number of particles in event: " << particleTree.size() << endl;

    // for(int i=1;i<particleTree.size();i++){ // Skipping the first element which was not dynamically allocated (the initial particle passed to this function)
        // delete particleTree[i];
    // }
}


void threadable(int id = 0, double depth = initialDepth, double energy = startingEnergy){
    if(file_exists("exitStatisticsOutputFile_t"+to_string(id)+".out")) {
        cerr << "exitStatisticsOutputFile_t"+to_string(id)+".out exists, cannot run thread id "+to_string(id)+"\n";
        activeThreads--;
        return;
    }

    std::mt19937_64 gen(random_seed<unsigned int>()); // Thread specific random number generator

    ofstream exitStatisticsOutputFile("exitStatisticsOutputFile_t"+to_string(id)+".out",ios::binary);
    for(int i=0;i<numIterationsPerThread;i++){
        quasiparticle auger(0, -1, getRandomStartingPosition(depth,gen), getRandomStartingVelocity(), energy, 0);
        runEvent(&auger, id, exitStatisticsOutputFile, gen);
        if(i<100 || (i<1000 && (i%10 == 0)) || (i<10000 && (i%100 == 0)) || (i<100000 && (i%1000 == 0)) || (i<1000000 && (i%10000 == 0)) || (i<10000000 && (i%100000 == 0)) || (i<100000000 && (i%1000000 == 0))) cout << "t" << setw(2) << id << ": Event " << i << " complete." << endl;
    }

    cout << "t" << setw(2) << id << ": Event " << numIterationsPerThread << " complete, exiting." << endl;
    exitStatisticsOutputFile.close();
    activeThreads--;
}






int main(int argc, char** argv){
    // if(file_exists("out.root")){
        // cerr << "File out.root exists. Aborting" << endl;
        // return -3;
    // }
    // TFile *f = new TFile("out.root","RECREATE");

    if(!readInPhononCouplingTable()){
        cerr << "Failed to read in phonon coupling table from CDFtab.csv. Aborting." << endl;
        return 2;
    }

    if(positionReadoutEnergy > 0 && timePositionReachout){
        cerr << "Cannot perform both time and energy position readouts. Aborting." << endl;
        return 4;
    }
    if(positionReadoutEnergy > 0 && file_exists("positionOut.txt")){
        cerr << "Position readout file (./positionOut.txt) already exists. Aborting." << endl;
        return 3;
    }
    if(positionReadoutEnergy > 0 || timePositionReachout) {
        outputPositionFile.open("positionOut.txt");
    }

    // Print simulation configuration:
    cout << "numIterationsPerThread = " << numIterationsPerThread << endl;
    cout << "maxThreads = " << maxThreads << endl;
    cout << "gottaGoFast = " << gottaGoFast << endl;
    cout << "timeOrderEvents = " << timeOrderEvents << endl;
    cout << "useStartingPhonons = " << useStartingPhonons << endl;
    cout << "timePositionReachout = " << timePositionReachout << endl;
    cout << "positionReadoutEnergy = " << positionReadoutEnergy/eV << " eV" << endl;
    cout << "minIonizationEnergy = " << minIonizationEnergy/eV << " eV" << endl;
    cout << endl;
    cout << "startingEnergy = " << startingEnergy/eV << " eV" << endl;;
    cout << "initialDepth = " << initialDepth/nm << " nm" << endl;
    cout << endl;
    cout << "workFunction = " << workFunction/eV << " eV" << endl;
    cout << "bindingEnergy = " << bindingEnergy/eV << " eV" << endl;
    cout << "phononVelocity = " << phononVelocity/(meter/second) << " meter/second" << endl;
    cout << "fermiVelocity = " << fermiVelocity/(meter/second) << " meter/second" << endl;
    cout << "halfSideLength = " << halfSideLength/micrometer << " um" << endl;
    cout << "halfHeight = " << halfHeight/nm << " nm" << endl;

    /*
    for(int depth = 0;depth<2;depth++){
        lostEnergyHistos.push_back(new TH1D(("lostEnergyHisto"+to_string(depth)+"nm").c_str(),("Auger Escape Energy at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy));
        escapedHistos.push_back(new TH2D(("escapedHisto"+to_string(depth)+"nm").c_str(),("Total Lost Energy vs Number of Escaped Electrons at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy,100,0,100));
    }*/
    double depth = initialDepth;

    /*
    for(int i=0;i<maxThreads;i++){
        lostEnergyHistos.push_back(new TH2D(("lostEnergyHisto"+to_string(depth)+"nm").c_str(),("Total Lost Energy vs Number of Escaped Electrons at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),560,0,startingEnergy,100,0,100));
        quasiparticleEnergy.push_back(new TH1D(("quasiparticleEnergy"+to_string(depth)+"nm").c_str(),("Final Total Quasiparticle Energy at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),10000,0,startingEnergy));
        quasiparticleNumber.push_back(new TH1D(("quasiparticleNumber"+to_string(depth)+"nm").c_str(),("Final Number of Quasiparticles at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),100000,0,100000)); //Estimate of how many particles we will have, may need to be updated));
        subGapPhononEnergy.push_back(new TH1D(("subGapPhononEnergy"+to_string(depth)+"nm").c_str(),("Final Total Sub-Gap Phonon Energy at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),10000,0,startingEnergy));
        subGapPhononNumber.push_back(new TH1D(("subGapPhononNumber"+to_string(depth)+"nm").c_str(),("Final Number of Sub-Gap Phonons at Implantation Depth of " + to_string(depth) + " nanometers").c_str(),100000,0,100000));

    }*/

    // Spawn all threads. Here is where I can configure different threads to run different depths and write to different histograms, but for rn it is just the one depth and the one set of histograms.
    sleep(1);
    vector<thread> threads;
    // if(argc == 2) threads.push_back(thread(threadable,stoi(argv[1]),depth,startingEnergy));
    // else abort();

    // for(int i=56;i>0;i--){
        // while(activeThreads >= maxThreads) sleep(1); // Wait for available thread
        // activeThreads++;
        // threads.push_back(thread(threadable,i,depth,/*startingEnergy*/i*eV));
    // }
    for(int i=0;i<1;i++){
        while(activeThreads >= maxThreads) sleep(1); // Wait for available thread
        activeThreads++;
        threads.push_back(thread(threadable,i,depth,startingEnergy));
    }
    // for(int i=1;i<16;i++){
        // while(activeThreads >= maxThreads) sleep(1); // Wait for available thread
        // activeThreads++;
        // threads.push_back(thread(threadable,i,4*i,startingEnergy));
    // }
    // for(int i=0;i<40;i++){
        // while(activeThreads >= maxThreads) sleep(1); // Wait for available thread
        // activeThreads++;
        // threads.push_back(thread(threadable,i,i+1,startingEnergy));
    // }

    for (std::thread & th : threads) th.join();

    // threadable(0,depth,startingEnergy); // Note the position outfile is not threadable


    // for(int i=0;i<lostEnergyHistos.size();i++){
    //     lostEnergyHistos[i]->Write(lostEnergyHistos[i]->GetName(), TObject::kSingleKey);
    //     quasiparticleEnergy[i]->Write(quasiparticleEnergy[i]->GetName(), TObject::kSingleKey);
    //     quasiparticleNumber[i]->Write(quasiparticleNumber[i]->GetName(), TObject::kSingleKey);
    //     subGapPhononEnergy[i]->Write(subGapPhononEnergy[i]->GetName(), TObject::kSingleKey);
    //     subGapPhononNumber[i]->Write(subGapPhononNumber[i]->GetName(), TObject::kSingleKey);
    //     delete lostEnergyHistos[i];
    //     delete quasiparticleEnergy[i];
    //     delete quasiparticleNumber[i];
    //     delete subGapPhononEnergy[i];
    //     delete subGapPhononNumber[i];
    //
    // }
    // f->Write();
    // f->Close();
    // delete f;

    outputPositionFile.close();

    return 0;
}
